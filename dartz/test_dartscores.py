

from unittest import skip, TestCase

from google.appengine.api import memcache
from google.appengine.ext import ndb
from google.appengine.ext import testbed
from google.appengine.ext import db

from dartscores import DartScores
from dartgames import Dart501Games, Player

class DartTestCase(TestCase):
    __name__ = 'DartTestCase'
    def setUp(self):
        self.testbed = testbed.Testbed()
        self.testbed.activate()
        self.testbed.init_user_stub()
        self.testbed.init_datastore_v3_stub()
        self.testbed.init_memcache_stub()
        ndb.get_context().clear_cache()

    def tearDown(self):
        self.testbed.deactivate()

    def test_get_darts(self):
        scores = DartScores()
        score_dict = scores.get_scores()
        assert score_dict[2] == [[2]]
        print scores.get_remaining(21)
        print scores.get_remaining(21,1)
        print scores.get_remaining(21,6,5)

    def test_get_all_darts_for_turn(self):
        scores = DartScores()
        all_scores = scores.get_all_possible_for_turn()
        print all_scores

    def test_get_remaingdarts(self):
        scores = DartScores()
        score_dict = scores.get_scores()
        print scores.get_remaining(134)
        print scores.get_remaining(21,1)

    def test_new_501_game(self):
        game = Dart501Games().new_game('jon','dr')
        assert game.created != None
        #score some darts
        assert game.get_score('jon') == 434
        game.score(160,160)
        game.score(180,180)
        try:
            game.score('jon',100)
        except ValueError as e:
            assert e.message == "Bust"
        assert game.get_score('jon') == 94
        game.score(20,30)

        try:        
            game.score_both(789,190)
            self.fail()
        except ValueError:
            pass
        try:        
            game.score(0,-1)
            self.fail()
        except ValueError:
            pass
        try:        
            game.score(0,1990)
            self.fail()
        except ValueError:
            pass
        try:        
            game.score(0,-1)
            self.fail()
        except ValueError:
            pass

        game.score('dr',"")

        try:        
            game.score('jon',' ')
            self.fail()
        except ValueError:
            pass

        game.score(None,None)
        game.score('dr',None)

        game.score('jon',0)
        assert game.get_score('jon') == 74
        assert game.get_score('dr') == 131

    def test_saving_501_game(self):
        game = Dart501Games().new_game('jon','dr')
        Dart501Games().score(game.key(),22,23)
        query = "SELECT * FROM Game ORDER BY created DESC"
        games = db.GqlQuery(query)
        print str(games.get())
        gid = games[0].key()
        print "gid --- "+str(gid)
        game = Dart501Games().get_game(gid)
        print game.get_score('jon')
        assert game.get_score('jon') == 478





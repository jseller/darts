
import logging

from google.appengine.api import memcache

class DartScores(object):
    """
    how many score in darts can you dart out on?
    must dart out on a double
    """
    def __init__(self): 
        # could use a set for ordering the resulting scores, but instead 
        # using the keys from the dict since it will end up being the same unique values
        self.scores = [0]
        self.scores.extend([x+1 for x in xrange(20)])
        self.double_scores = [x * 2 for x in self.scores]
        triple_scores = [x * 3 for x in self.scores]
        self.possible_scores = set(self.scores).union(self.double_scores).union(triple_scores)
        print("self.possible: "+str(self.possible_scores))
        self.all_scores = {}
        
    def init_scores(self):
        logging.info('initializing dart scores')
        self.all_scores = {}
        for first_dart in self.double_scores:
            self.add_to_scores(first_dart)
            for second_dart in self.possible_scores:
                self.add_to_scores(first_dart, second_dart)
                for third_dart in self.possible_scores:
                    self.add_to_scores(first_dart, second_dart, third_dart)

    def add_to_scores(self, dart, second_dart=0, third_dart=0):
        idx = dart + second_dart + third_dart
        res = None
        if second_dart > 0 and third_dart > 0:
            res = [third_dart, second_dart, dart]
        elif second_dart > 0:
            res = [second_dart, dart]
        else:
            res = [dart]
        if res:
            if self.all_scores.get(idx):
                self.all_scores[idx].append(res)
            else:
                self.all_scores[idx] = [res]

    def get_scores(self):
        if not self.all_scores:
            self.all_scores = memcache.get("dart_scores")
            if not self.all_scores:
                self.init_scores()
                memcache.add(key="dart_scores", value=self.all_scores, time=3600)
        return self.all_scores

    def valid_score(self, score):
        if score:
            try:
                return int(score)
            except ValueError:
                pass

    def get_remaining(self, score, first=None, second=None):
        score = self.valid_score(score)
        first = self.valid_score(first)
        second = self.valid_score(second)
        all_scores = self.get_scores()
        score_dict = all_scores[score]
        score_dict.sort(key=lambda x: -x[0])
        ret_scores = []
        if first and second:
            for scores in score_dict:
                if scores[0] == first and scores[1] == second:
                    ret_scores.append(scores)
            return ret_scores
        elif first:
            for scores in score_dict:
                if scores[0] == first:
                    ret_scores.append(scores)
            return ret_scores
        else:
            return score_dict


    def compute_all_possible_for_turn(self):
        all = set([])
        ascore = 0
        for first_dart in self.possible_scores:
            for second_dart in self.possible_scores:
                for third_dart in self.possible_scores:
                    ascore = first_dart + second_dart + third_dart
                    all.add(ascore)
                    ascore = 0
        print("all.scoresturn: "+str(all))
        return all


    def get_all_possible_for_turn(self):
        key = "all_for_turn"
        turn = memcache.get(key)
        if not turn:
            turn = self.compute_all_possible_for_turn()
            memcache.add(key=key, value=turn, time=3600)
        return turn


    def print_desc(self):
        print str(self.get_scores())
        dict_ids = [x for x in reversed(self.all_scores.keys())]
        print dict_ids

'''
import json
f = open('dartresults.json','w')
message = json.dumps(desc)
f.write(message)
f.close()
'''

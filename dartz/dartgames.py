
import logging
from datetime import datetime

from google.appengine.ext import db
from google.appengine.api.datastore_errors import BadKeyError

from dartscores import DartScores

scores = DartScores()

# player has a list of scores during the game
class Player(db.Model):
    name = db.StringProperty()
    created = db.DateTimeProperty() 

    def to_dict(self):
        return {'name':name}

# game has a list of players
class Game(db.Model):
    player1 = db.StringProperty()
    player1_score = db.IntegerProperty()
    player2 = db.StringProperty()
    player2_score = db.IntegerProperty()
    player1_darts = db.StringProperty()
    player2_darts = db.StringProperty()
    next = db.StringProperty()
    created = db.DateTimeProperty()

    def __str__(self):
        return str(self.to_dict())

    def to_dict(self):
        return dict([(p, unicode(getattr(self, p))) for p in self.properties()])

    def valid_score(self, score, points):
        pi = int(points) 
        if pi not in scores.get_all_possible_for_turn():
            raise ValueError("%s - Need valid number between 0-180 "%(str(points)))
        if pi < 0:
            raise ValueError("%s - Need number between 0-180 "%(str(points)))
        if pi > 180:
            raise ValueError("%s - Need number between 0-180 "%(str(points)))
        player_score = int(score) - pi
        if player_score == 1:
            raise ValueError("Cannot check on 1")
        if player_score < 0:
            raise ValueError("Bust")
        if player_score == 0 and score not in scores.double_scores:
            raise ValueError("Bust, need to check on a double score")
        return player_score

    def p1_score(self, points):
        self.player1_score = self.valid_score(self.player1_score, points)
        self.next = self.player2
        self.player1_darts += ","+str(points)
        
    def p2_score(self, points):
        self.player2_score = self.valid_score(self.player2_score, points)
        self.next = self.player1
        self.player2_darts += ","+str(points)

    def score(self, points1, points2):
        if points1:
            self.p1_score(points1)
        if points2:
            self.p2_score(points2)

    def get_score(self, player):
        if self.player1 == player:
            return self.player1_score
        if self.player2 == player:
            return self.player2_score

class Dart501Games(object):

    def new_game(self, player1, player2):
        game = Game()
        if not player1 or not player2:
           raise ValueError("Need two players")

        game.player1 = player1
        game.player1_score = 501
        game.player1_darts = ""
        game.player2 = player2
        game.player2_score = 501
        game.player2_darts = ""
        game.total_darts = 0
        game.created = datetime.utcnow()
        game.next = player1
        game.put()
        return game

    def get_game(self, game_id):
        try:
            if game_id:
                return db.get(game_id)
        except BadKeyError:
            pass

    def score_both(self, game_id, player1, player2):
        game = self.get_game(game_id)
        if game:
            game.score_both(player1, player2)
            game.put()
            return game

    def score(self, game_id, player, points):
        print game_id
        game = self.get_game(game_id)
        if game:
            game.score(player, points)
            game.put()
            return game





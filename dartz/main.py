#!/usr/bin/env python
import os
import urllib
import logging

from google.appengine.api import users
from google.appengine.ext import ndb
from google.appengine.ext import db

import jinja2
import webapp2

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

from dartscores import DartScores
from dartgames import Dart501Games

class MainHandler(webapp2.RequestHandler):
    def get(self):
        errors = []

        # Checks for active Google account session
        user = users.get_current_user()
        if user:
            url = users.create_logout_url(self.request.uri)
            url_linktext = 'Logout'
            user_name = user.nickname()
        else:
            url = users.create_login_url(self.request.uri)
            url_linktext = 'Login'
            user_name = None

        greetings =  'Hello'
        if user_name:
            greetings +=', '+user_name

        game = None
        gid = self.request.get('gid')
        if gid:
           game = Dart501Games().get_game(gid)
           if game is None:
               errors.append("invalid game key")

        games = db.GqlQuery("SELECT * FROM Game ORDER BY created DESC LIMIT 5")

        if '/check' in self.request.uri:
            root_url = self.request.uri.split('chec')[0]
        elif '?' in self.request.uri:
            root_url = self.request.uri.split('?')[0]
        else:
            root_url = self.request.uri

        template_values = {
            'user': user,
            'greetings': greetings,
            'guestbook_name': user_name,
            'game' : game,
            'games' : games,
            'url': url,
            'root_url' : root_url,
            'url_linktext': url_linktext,
            'errors' : errors,
        }

        template = JINJA_ENVIRONMENT.get_template('index.html')
        self.response.write(template.render(template_values))

from webapp2_extras import json

class RemainingHandler(webapp2.RequestHandler):
    def post(self):
        points = self.request.get('points')
        first = self.request.get('first')
        second = self.request.get('second')
        self.response.content_type = 'application/json'
        try:
            remaining = DartScores().get_remaining(points, first, second)
            obj = {
                'success': points, 
                'remaining': remaining
            } 
        except ValueError as e:
            obj = {
                'error': e.message,
            }

        self.response.write(json.encode(obj))

class Game501Handler(webapp2.RequestHandler):
    def post(self):
        game_id = self.request.get('gid')
        player1 = self.request.get('p1')
        player2 = self.request.get('p2')
        logging.info(game_id+" "+player1+" "+player2)

        try:
            game = Dart501Games().score(game_id, player1, player2)
            logging.info(str(game))
            obj = {
                'success': 200, 
                'game': game.to_dict()
            } 
        except ValueError as e:
            logging.error(e.message)
            obj = {
                'error': e.message,
            }
        self.response.content_type = 'application/json'
        self.response.write(json.encode(obj))
        

class NewGame501Handler(webapp2.RequestHandler):
    def post(self):
        player1 = self.request.get('player1')
        player2 = self.request.get('player2')
        game = Dart501Games().new_game(player1, player2)
        self.redirect("/?gid="+str(game.key()))
       

app = webapp2.WSGIApplication([
    ('/', MainHandler),
    ('/remaining', RemainingHandler),
    ('/score', Game501Handler),
    ('/new', NewGame501Handler),
], debug=True)

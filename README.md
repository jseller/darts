
Details of the users, stories, architecture and versions:

https://bitbucket.org/jseller/darts/wiki/

# README #
* Test
run 'test' from the dartz directory, this will run python unittests and functional tests
deploy on google appengine 

### How do I get set up? ###
* Deploy to google appengine, configuation is dynamic for root url, datasctore and memcached
* How to run tests: just run "test" and the batch file (windows) or shell script (linux, mac) will run unit tests and functional tests

### Contribution guidelines ###

* Writing tests
Write tests for classes the 'test_' convention. setup method for GAE dependencies
* Code review
https://google.github.io/styleguide/pyguide.html
* Other guidelines

### Who do I talk to? ###

* jseller
* Other community or team contact